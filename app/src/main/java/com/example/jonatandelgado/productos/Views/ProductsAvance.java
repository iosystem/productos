package com.example.jonatandelgado.productos.Views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;

import com.example.jonatandelgado.productos.Adapter.ProductAvanceAdapter;
import com.example.jonatandelgado.productos.Model.Product;
import com.example.jonatandelgado.productos.R;

import java.util.ArrayList;

public class ProductsAvance extends AppCompatActivity implements AdapterView.OnItemClickListener {

    //private String [] lista = {"hombre","mujer","niña"};
    //private ListView list_view;
    private ProductAvanceAdapter adapter;
    private RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_avance);
        recyclerView = (RecyclerView) findViewById(R.id.miList);
        adapter = new ProductAvanceAdapter(new ArrayList<Product>());
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        //list_view = (ListView) findViewById(R.id.miList);
        //  adapter;
        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lista);
        //list_view.setAdapter(adapter);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this,ProductDetails.class));
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }
}
