package com.example.jonatandelgado.productos.Interface;

import java.util.ArrayList;
import java.util.Date;

public abstract class IProduct {


    public abstract ArrayList<Object> productos_activos();

    public abstract ArrayList<Object> productos_inactivos();

    public abstract void buscar_cantidad_de_reorden();


}
