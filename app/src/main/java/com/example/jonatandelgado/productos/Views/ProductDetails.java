package com.example.jonatandelgado.productos.Views;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.SQLException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Path;
import android.graphics.drawable.Drawable;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jonatandelgado.productos.DB.DbOperation;
import com.example.jonatandelgado.productos.Enum.Category;
import com.example.jonatandelgado.productos.R;
import com.example.jonatandelgado.productos.Model.Product;
import com.microsoft.appcenter.analytics.Analytics;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Hashtable;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;


public class ProductDetails extends AppCompatActivity  {

    private AppCompatImageView picture;
    private Button btnSave;
    private Button btnTakePicture;
    private Button btnUploadPicture;
    private Button btnDelete;
    private Button btnEdit;
    private TextView txtId;
    private TextView txtName;
    private TextView txtDescription;
    private TextView txtPrice;
    private TextView txtStock;
    private Spinner cmbCategory;
    private DbOperation operation;
    private String path = "";
    private boolean isEditable= false;
    private Product product = new Product();
    private final String CARPETA_RAIZ ="ImagenProductManager/";
    private final String RUTA_IMAGEN =CARPETA_RAIZ+"TomaDeCamara";
    private final int CODE_TAKE_PHOTO = 10;
    private final int CODE_LOAD_PHOTO = 5;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        Analytics.trackEvent("Entran a detalle de producto");
        getPermission();
        operation = new DbOperation(this);

        picture = (AppCompatImageView) findViewById(R.id.imgProduct);
        txtId = (TextView) findViewById(R.id.txtId);
        txtName = (TextView) findViewById(R.id.txtName);
        txtDescription = (TextView) findViewById(R.id.txtDescripcion);
        txtPrice = (TextView) findViewById(R.id.txtPrice);
        txtStock = (TextView) findViewById(R.id.txtStock);

        cmbCategory = (Spinner) findViewById(R.id.cmbCategory);

        btnUploadPicture = (Button) findViewById(R.id.btnUploadPicture);
        btnTakePicture = (Button) findViewById(R.id.btnTakePicture);
        btnSave = (Button) findViewById(R.id.btnSave);
        btnDelete = (Button) findViewById(R.id.btnDelete);
        btnEdit = (Button) findViewById(R.id.btnEdite);

        cmbCategory.setAdapter(new ArrayAdapter<String>(getApplicationContext(),
                android.R.layout.simple_spinner_dropdown_item,
                operation.allNameCategory()));

        Bundle dataSent = getIntent().getExtras();

        if(dataSent != null)
        {
            try {
                product = (Product) dataSent.getSerializable("Product");
                loadData(product);
            }
            catch ( Exception ex)
            {
                Log.d("Error al cargar data",ex.getMessage());
            }
        }

        btnUploadPicture.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                uploadImage();
            }
        });

        btnTakePicture.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                takePicture();
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                product.setNombre(txtName.getText().toString());
                product.setDescripcion(txtDescription.getText().toString());
                product.setPrecio(Double.parseDouble( txtPrice.getText().toString()));
                product.setCantidad(Integer.parseInt( txtStock.getText().toString()));
                setIdCategoria(cmbCategory.getSelectedItem().toString());

                if(path != "" && path != null)
                    product.setImagen(path);
                if(!isEditable) {
                    try {
                        Analytics.trackEvent("Guarda producto nuevo.");
                        operation.insertProduct("Product", product);
                        Toast.makeText(getApplicationContext(), "Datos guardados!", Toast.LENGTH_SHORT).show();
                        txtName.setText("");
                        txtDescription.setText("");
                        txtPrice.setText("");
                        txtStock.setText("");
                        Drawable idImagen = getResources().getDrawable(R.drawable.default_picture);
                        picture.setImageDrawable(idImagen);
                    } catch (Exception ex) {
                        Log.d("Error DB", ex.getMessage());
                    }
                }
                else
                {
                    try
                    {
                        Analytics.trackEvent("Guarda al momento de editar producto.");
                        product.setCodigo(Integer.parseInt( txtId.getText().toString()));
                        operation.updateProduct(product);
                        loadData(product);
                    }
                    catch (Exception ex)
                    {
                        Log.d("Error DB", ex.getMessage());
                    }
                }
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try{
                    AlertDialog.Builder confirmationMessage =
                            new AlertDialog.Builder(ProductDetails.this);

                    confirmationMessage.setMessage("¿Esta seguro que desea Eliminar el registro?")
                                        .setCancelable(false)
                    .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Analytics.trackEvent("Elimina producto.");
                            operation.deleteProduct(product.getCodigo());
                            finish();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    });

                    AlertDialog message = confirmationMessage.create();
                    message.setTitle("Eliminar Producto");
                    message.show();
                }
                catch (Exception ex)
                {
                    Log.d("Error DB", ex.getMessage());
                }
            }
        });
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Analytics.trackEvent("Edita producto.");
                isEditable = true;
                txtName.setEnabled(true);
                txtDescription.setEnabled(true);
                txtPrice.setEnabled(true);
                txtStock.setEnabled(true);
                cmbCategory.setEnabled(true);
                btnSave.setEnabled(true);
                btnTakePicture.setEnabled(true);
                btnUploadPicture.setEnabled(true);
                btnUploadPicture.setVisibility(View.VISIBLE);
                btnSave.setVisibility(View.VISIBLE);
                btnTakePicture.setVisibility(View.VISIBLE);
                btnEdit.setVisibility(View.GONE);
                btnDelete.setVisibility(View.GONE);
            }
        });
    }

    private void loadData(Product product){
        try
        {
            txtId.setText(String.valueOf(product.getCodigo()));
            txtName.setText(product.getNombre());
            txtDescription.setText(product.getDescripcion());
            txtPrice.setText(String.valueOf(product.getPrecio()));
            txtStock.setText( String.valueOf(product.getCantidad()));
            if(!isEditable)
                cmbCategory.setSelection(product.getIdCategoria()-1);
            txtName.setEnabled(false);
            txtDescription.setEnabled(false);
            txtPrice.setEnabled(false);
            txtStock.setEnabled(false);
            cmbCategory.setEnabled(false);

            btnSave.setEnabled(false);
            btnTakePicture.setEnabled(false);
            btnUploadPicture.setEnabled(false);
            btnUploadPicture.setVisibility(View.GONE);
            btnSave.setVisibility(View.GONE);
            btnTakePicture.setVisibility(View.GONE);
            btnEdit.setVisibility(View.VISIBLE);
            btnDelete.setVisibility(View.VISIBLE);

            String path = product.getImagen();
            if(path != null) {
                if (!path.contains("content")) {
                    // Get the dimensions of the View
                    int targetW = picture.getWidth() == 0 ? 450 : picture.getWidth();
                    int targetH = picture.getHeight() == 0 ? 450 : picture.getHeight();

                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    bmOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(path, bmOptions);
                    int photoW = bmOptions.outWidth;
                    int photoH = bmOptions.outHeight;
                    // Determine how much to scale down the image
                    int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
                    // Decode the image file into a Bitmap sized to fill the View
                    bmOptions.inJustDecodeBounds = false;
                    bmOptions.inSampleSize = scaleFactor;
                    bmOptions.inPurgeable = true;

                    Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);
                    picture.setImageBitmap(bitmap);
                } else {
                    Bitmap bitmap = getBitmapForUploadMedia(path);
                    picture.setImageBitmap(bitmap);
                }
            }
            else
            {
                Log.d("Imagen no guardada","algo paso");
            }
        }
        catch (Exception ex)
        {
            Log.d("Error al cargar data.",ex.getMessage());
        }
    }

    private Bitmap getBitmapForUploadMedia(String path) throws IOException {
        Uri myUri = Uri.parse(path);
        InputStream inputStream = this.getContentResolver().openInputStream(myUri);
        Bitmap bmp = BitmapFactory.decodeStream(inputStream);
        if( inputStream != null ) inputStream.close();

        return bmp;
    }

    private void getPermission() {
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},100);
                // MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    private void takePicture(){
        getPermission();
        File fileImage = new File(Environment.getExternalStorageDirectory(),RUTA_IMAGEN);
        boolean isCreated = fileImage.exists();
        String nameImagen = "";

        if(isCreated == false)
        {
            isCreated= fileImage.mkdirs();
        }
        else
        {
            nameImagen = (System.currentTimeMillis()/1000)+".jpg";
        }
        path = Environment.getExternalStorageDirectory() +
                File.separator+RUTA_IMAGEN+File.separator+nameImagen;
        File image = new File(path);

        Intent intent =new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        //Forma para validar la version y poder usar el metodo adecuado
        if(Build.VERSION.SDK_INT >=Build.VERSION_CODES.N)
        {
            String authorities =getApplicationContext().getPackageName()+".provider";
            Uri imagenUri = FileProvider.getUriForFile(this,authorities,image);
            intent.putExtra(MediaStore.EXTRA_OUTPUT,imagenUri);
        }
        else
        {
            intent.putExtra(MediaStore.EXTRA_OUTPUT,Uri.fromFile(image));
        }
        startActivityForResult(intent,CODE_TAKE_PHOTO);
    }

    private void uploadImage(){
        Intent intent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/jpeg");
        startActivityForResult(intent.createChooser(intent,"Seleccione la Aplicación"),CODE_LOAD_PHOTO);
    }

    private  void setIdCategoria(String nameCategory) {
        switch (nameCategory)
        {
            case "Groceries":
                product.setIdCategoria(1);
                break;
            case "Cars":
                product.setIdCategoria(2);
                break;
            case "Sport":
                product.setIdCategoria(3);
                break;
            case "Beer":
                product.setIdCategoria(4);
                break;
            case "Phones":
                product.setIdCategoria(5);
                break;
            case "TV":
                product.setIdCategoria(6);
                break;
        }
    }

    private boolean validationsField(){
        boolean isValid = true;


        return isValid;
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();

        setResult(RESULT_CANCELED);
        //codigo adicional

        this.finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK)
        {
            switch (requestCode)
            {
                case CODE_LOAD_PHOTO:
                    Uri myPath= data.getData();
                    path = data.getDataString();
                    picture.setImageURI(myPath);
                break;
                case CODE_TAKE_PHOTO:
                    MediaScannerConnection.scanFile(getApplicationContext(),new String[]{path},null,
                            new MediaScannerConnection.OnScanCompletedListener(){
                                @Override
                                public void onScanCompleted(String path, Uri uri) {

                                }
                            });
                    // Get the dimensions of the View
                    int targetW = picture.getWidth();
                    int targetH = picture.getHeight();
                    // Get the dimensions of the bitmap
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    bmOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(path, bmOptions);
                    int photoW = bmOptions.outWidth;
                    int photoH = bmOptions.outHeight;
                    // Determine how much to scale down the image
                    int scaleFactor = Math.min(photoW/targetW, photoH/targetH);
                    // Decode the image file into a Bitmap sized to fill the View
                    bmOptions.inJustDecodeBounds = false;
                    bmOptions.inSampleSize = scaleFactor;
                    bmOptions.inPurgeable = true;

                    Bitmap bitmap = BitmapFactory.decodeFile(path,bmOptions);
                    picture.setImageBitmap(bitmap);
                break;
            }
        }
    }

}
