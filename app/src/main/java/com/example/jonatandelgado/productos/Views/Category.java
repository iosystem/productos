package com.example.jonatandelgado.productos.Views;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.example.jonatandelgado.productos.DB.DbOperation;
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

import com.example.jonatandelgado.productos.Adapter.CategoryAdapter;
import com.example.jonatandelgado.productos.R;
import com.microsoft.appcenter.distribute.Distribute;


public class Category extends AppCompatActivity {

    private GridView gridView;
    private DbOperation operation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);
        // Implementacion de appCenter
        AppCenter.start(getApplication(), "aa4d7a47-70cc-476d-a621-dbe17946936d",
                Analytics.class, Crashes.class,Distribute.class);
        AppCenter.setLogLevel(Log.VERBOSE);

        Analytics.trackEvent("Entra en categoria");
        operation = new DbOperation(this);

        gridView = (GridView) findViewById(R.id.Gridview);
        CategoryAdapter adapter = new CategoryAdapter(getApplicationContext(),operation.allCategory());
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intent = new Intent(Category.this,Product.class);
                TextView idcategory = view.findViewById(R.id.textIdCategory);

                intent.putExtra("categoryID",idcategory.getText());
                startActivity(intent);
            }
        });
    }
}
