package com.example.jonatandelgado.productos.Views;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jonatandelgado.productos.DB.DbOperation;
import com.example.jonatandelgado.productos.R;
import com.microsoft.appcenter.analytics.Analytics;

public class Login extends AppCompatActivity {

    private Button btnLogin;
    private TextView txtName;
    private TextView txtPass;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        txtName = (TextView) findViewById(R.id.textNameLogin);
        txtPass = (TextView) findViewById(R.id.textPassword);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    DbOperation db = new DbOperation(getApplicationContext());
                    boolean result;
                    db.OpenDB();
                    result = db.login(txtName.getText().toString(),txtPass.getText().toString());
                    if(!result)
                    {
                        txtName.setText("");
                        txtPass.setText("");
                        Toast.makeText(getApplicationContext(),
                                "Usuario o password no son validos",
                                Toast.LENGTH_SHORT).show();
                    }
                    else{
                        Intent intent = new Intent(Login.this,Category.class);
                        startActivity(intent);
                        finish();
                    }

                    db.CloseDB();
                } catch (Exception ex)
                {
                    Log.e("Error DB",ex.getMessage());
                }

            }
        });
    }


}
