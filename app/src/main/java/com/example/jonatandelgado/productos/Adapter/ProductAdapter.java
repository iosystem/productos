package com.example.jonatandelgado.productos.Adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jonatandelgado.productos.Model.Product;
import com.example.jonatandelgado.productos.R;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

public class ProductAdapter extends BaseAdapter  {

    protected ArrayList<Product> products;
    private LayoutInflater layoutInflater;
    private  Context context;
    public ProductAdapter ( Context context,ArrayList<Product> products) {
        this.products = products;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        try {
            view = layoutInflater.inflate(R.layout.row_product, null);

            TextView textName = (TextView) view.findViewById(R.id.textName);
            TextView textPrice = (TextView) view.findViewById(R.id.textPrice);
            ImageView imgPicture = (ImageView) view.findViewById(R.id.imgPicture);
            textName.setText(products.get(position).getNombre());
            textPrice.setText("$ "+products.get(position).getPrecio());
            String path = products.get(position).getImagen();
            if(path != null) {
                if (!path.contains("content")) {
                    // Get the dimensions of the View
                    int targetW = imgPicture.getWidth() == 0 ? 450 : imgPicture.getWidth();
                    int targetH = imgPicture.getHeight() == 0 ? 450 : imgPicture.getHeight();

                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    bmOptions.inJustDecodeBounds = true;
                    BitmapFactory.decodeFile(path, bmOptions);
                    int photoW = bmOptions.outWidth;
                    int photoH = bmOptions.outHeight;
                    // Determine how much to scale down the image
                    int scaleFactor = Math.min(photoW / targetW, photoH / targetH);
                    // Decode the image file into a Bitmap sized to fill the View
                    bmOptions.inJustDecodeBounds = false;
                    bmOptions.inSampleSize = scaleFactor;
                    bmOptions.inPurgeable = true;

                    Bitmap bitmap = BitmapFactory.decodeFile(path, bmOptions);
                    imgPicture.setImageBitmap(bitmap);
                } else {
                    Bitmap bitmap = getBitmapForUploadMedia(path);
                    imgPicture.setImageBitmap(bitmap);
                }
            }
            else
            {
                Drawable idImagen = view.getResources().getDrawable(R.drawable.default_picture);
                imgPicture.setImageDrawable(idImagen);
            }
            return view;
        }
        catch (Exception ex)
        {
            return  null;
        }
    }

    private Bitmap getBitmapForUploadMedia(String path) throws IOException {
        Uri myUri = Uri.parse(path);
        InputStream inputStream = context.getContentResolver().openInputStream(myUri);
        Bitmap bmp = BitmapFactory.decodeStream(inputStream);
        if( inputStream != null ) inputStream.close();

        return bmp;
    }

}
