package com.example.jonatandelgado.productos.Views;

import android.content.Intent;
import android.graphics.Color;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.jonatandelgado.productos.Adapter.ProductAdapter;
import com.example.jonatandelgado.productos.DB.DbOperation;
import com.example.jonatandelgado.productos.R;
import com.example.jonatandelgado.productos.Views.ProductDetails;
import com.microsoft.appcenter.analytics.Analytics;

import java.util.ArrayList;

public class Product extends AppCompatActivity  {
    private String [] lista = {"hombre","mujer","niña"};
    private ListView list_view;
    private FloatingActionButton btnFloatAdd;
    private SwipeRefreshLayout swiperefresh;
    private DbOperation operation;
    private int idCategory = 0;
    private ArrayList<com.example.jonatandelgado.productos.Model.Product> products;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        Analytics.trackEvent("Entran a listado de Producto");

        operation = new DbOperation(this);
        idCategory = Integer.parseInt((getIntent().getStringExtra("categoryID")));
        products = operation.allProductByID(idCategory);

        swiperefresh = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
        list_view = (ListView) findViewById(R.id.miList);
        btnFloatAdd = (FloatingActionButton) findViewById(R.id.addProduct);
        //refreshData();

        ProductAdapter adapter = new ProductAdapter(getApplicationContext(),products);
        ArrayAdapter<String> adaptador;
        //Eventos para diferentes acciones de nuestra vista.
        //Accion de lista de elemento
        list_view.setAdapter(adapter );

        list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final int pos = position;
                Intent intent = new Intent(getApplicationContext(),ProductDetails.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("Product",products.get(position));
                intent.putExtras(bundle);
                startActivityForResult(intent,1);
            }
        });
        //Accion para boton flotante
        btnFloatAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(getApplicationContext(),ProductDetails.class),1);

            }
        });
        //Accion para el refresh
        swiperefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        refreshData();
                    }
                }
        );
    }

    private void refreshData(){
        products = null;
        products = operation.allProductByID(idCategory);
        list_view.setAdapter(null);
        ProductAdapter adapter = new ProductAdapter(getApplicationContext(),products);
        list_view.setAdapter(adapter);
        swiperefresh.setRefreshing(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        startActivity(new Intent(this,ProductDetails.class));
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_CANCELED )
        {
            refreshData();
        }

    }
}
