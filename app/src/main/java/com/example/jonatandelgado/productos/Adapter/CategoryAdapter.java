package com.example.jonatandelgado.productos.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jonatandelgado.productos.R;
import com.example.jonatandelgado.productos.Model.Category;

import java.util.ArrayList;
import java.util.Objects;

public class CategoryAdapter extends BaseAdapter {
    private Context context;
    private String listImage [] ;
    private  int Images [];
    private LayoutInflater layoutInflater;
    private ArrayList<Category> categories;

    public CategoryAdapter(Context context, ArrayList<Category> categories) {
        this.context = context;
        this.categories = categories;
        this.layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        try {
            view = layoutInflater.inflate(R.layout.row_category, null);
            ImageView image = (ImageView) view.findViewById(R.id.imageBrand);
            TextView descriptionImage = (TextView)view.findViewById(R.id.NameCategory);
            TextView idCategory = (TextView)view.findViewById(R.id.textIdCategory);
            descriptionImage.setText(categories.get(position).getName());
            idCategory.setText( Long.toString(categories.get(position).getId()));
            image.setImageResource(categories.get(position).getPathImage());

            return view;
        }catch (Exception  x)
        {
            return  null;
        }
    }
}
