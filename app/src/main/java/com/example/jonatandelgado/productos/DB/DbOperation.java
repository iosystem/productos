package com.example.jonatandelgado.productos.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.Nullable;

import com.example.jonatandelgado.productos.Model.Category;
import com.example.jonatandelgado.productos.Model.Product;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;

public class DbOperation extends DbContext {
    public static final String NAME ="ProductManager";
    public static final int VERSION = 1;

    public DbOperation(Context context) {
        super(context, NAME, null, VERSION);
    }
    public boolean login(String user, String pass) {

        try {
            Cursor cursor = null;
            cursor = this.getReadableDatabase()
                    .query("User",
                            new String []{"Name","Pass"},
                            "Name =? and Pass = ?",
                            new String[] {user,pass},null,null,null
                    );

            return cursor.getCount() > 0? true: false;
        }
        catch (Exception ex) {
            throw ex;
        }
    }
    public void insertProduct(String tableName, Product product){
        try {
            this.OpenDB();
            ContentValues newInsert = new ContentValues();
            newInsert.put("Name",product.getNombre());
            newInsert.put("Description",product.getDescripcion());
            newInsert.put("Price",product.getPrecio());
            newInsert.put("Path_image",product.getImagen());
            newInsert.put("ID_Category",product.getIdCategoria());
            newInsert.put("Stock",product.getCantidad());
            this.getWritableDatabase().insert(tableName, null, newInsert);
            this.CloseDB();
        }
        catch (Exception ex)
        {
            throw  ex;
        }
    }
    public void deleteProduct(int id) throws SQLException {
        try {
            this.OpenDB();
            Cursor cursor = null;
            this.getWritableDatabase()
                    .delete("Product", " id= ?",
                            new String[]{String.valueOf(id)});
        }
        catch (Exception ex)
        {
            throw  ex;
        }
    }
    public void updateProduct(Product product) {
        try {
            this.OpenDB();
            ContentValues newInsert = new ContentValues();
            newInsert.put("Name",product.getNombre());
            newInsert.put("Description",product.getDescripcion());
            newInsert.put("Price",product.getPrecio());
            newInsert.put("Path_image",product.getImagen());
            newInsert.put("ID_Category",product.getIdCategoria());
            newInsert.put("Stock",product.getCantidad());

            this.getWritableDatabase().update("Product",
                    newInsert,
                    "ID ="+product.getCodigo(),
                    null);
            this.CloseDB();
        }
        catch (Exception ex)
        {
            throw  ex;
        }
    }
    public ArrayList<Product> allProduct() throws SQLException {
        Cursor cursor = null;
        ArrayList<Product> products = new ArrayList<Product>();
        cursor = this.getReadableDatabase()
                .rawQuery("select * from Product",null);

        while (cursor.moveToNext())
        {
            Product product = new Product();
            product.setCodigo(cursor.getInt(0));
            product.setNombre(cursor.getString(1));
            product.setDescripcion(cursor.getString(2));
            product.setPrecio(cursor.getDouble(3));
            product.setImagen(cursor.getString(4));
            product.setIdCategoria(cursor.getInt(5));
            product.setCantidad(cursor.getInt(6));
            products.add(product);
        }
        return products;
    }
    public ArrayList<Category> allCategory() throws SQLException {
        Cursor cursor = null;
        ArrayList<Category> categories = new ArrayList<Category>();
        cursor = this.getReadableDatabase()
                .rawQuery("select * from Category",null);

        while (cursor.moveToNext())
        {
            Category category = new Category();
            category.setId(cursor.getInt(0));
            category.setName(cursor.getString(1));
            category.setPathImage(cursor.getInt(2));
            categories.add(category);
        }
        return categories;
    }
    public ArrayList<String> allNameCategory() throws SQLException {
        Cursor cursor = null;
        ArrayList<String> categories = new ArrayList<String>();
        cursor = this.getReadableDatabase()
                .rawQuery("select * from Category",null);

        while (cursor.moveToNext())
        {
            categories.add(cursor.getString(1));
        }
        return categories;
    }
    public Product findProduct(int id) throws SQLException {
        try {
            Cursor cursor = null;

            cursor = this.getReadableDatabase()
                    .query("Product",
                     new String []{"ID","Name","Description","Price","Path_image","ID_Category","Stock"},
                    "ID_Category =?",
                    new String[] {Integer.toString(id)},null,null,null
            );
            Product product = new Product();
            while (cursor.moveToNext())
            {

                product.setCodigo(cursor.getInt(0));
                product.setNombre(cursor.getString(1));
                product.setDescripcion(cursor.getString(2));
                product.setPrecio(cursor.getDouble(3));
                product.setImagen(cursor.getString(4));
                product.setCantidad(cursor.getInt(5));
            }
           return  product;
        }
        catch (Exception ex)
        {
            throw  ex;
        }
    }
    public ArrayList<Product>  allProductByID(int id) throws SQLException {
        try {
            this.OpenDB();
            Cursor cursor = null;
            ArrayList<Product> products = new ArrayList<Product>();
            String selectQuery = "SELECT  * FROM Product where ID_Category = "+id;
            //String selectQuery = "SELECT  * FROM Product ";
            cursor = this.getReadableDatabase().rawQuery(selectQuery, null);

            while (cursor.moveToNext())
            {
                Product product = new Product();
                product.setCodigo(cursor.getInt(0));
                product.setNombre(cursor.getString(1));
                product.setDescripcion(cursor.getString(2));
                product.setPrecio(cursor.getDouble(3));
                product.setImagen(cursor.getString(4));
                product.setIdCategoria(cursor.getInt(5));
                product.setCantidad(cursor.getInt(6));
                products.add(product);
            }
            return products;

        }
        catch (Exception ex)
        {
            throw  ex;
        }
    }

    public void update(){
        try {
            this.OpenDB();

            this.CloseDB();
        }
        catch (Exception ex)
        {
            throw  ex;
        }
    }
    public void delete(){
        try {
            this.OpenDB();

            this.CloseDB();
        }
        catch (Exception ex)
        {
            throw  ex;
        }
    }
}
