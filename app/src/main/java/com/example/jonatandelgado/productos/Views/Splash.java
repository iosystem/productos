package com.example.jonatandelgado.productos.Views;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.example.jonatandelgado.productos.DB.DbContext;
import com.example.jonatandelgado.productos.DB.DbOperation;
import com.example.jonatandelgado.productos.R;

public class Splash extends AppCompatActivity {
    // Duración en milisegundos que se mostrará el splash
    private final int DURACION_SPLASH = 3000; // 3 segundos
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        DbOperation db = new DbOperation(this);
        try {
            db.OpenDB();
            db.insertDefaultCategory();
            db.insertDefaultUser();
            db.CloseDB();
        } catch (Exception ex)
        {
            Log.e("Error DB",ex.getMessage());
        }
        new Handler().postDelayed(new Runnable(){
            public void run(){

                // Cuando pasen los 3 segundos, pasamos a la actividad principal de la aplicación
                Intent intent = new Intent(Splash.this, Login.class);
                startActivity(intent);
                finish();
            };
        }, DURACION_SPLASH);

    }

}
