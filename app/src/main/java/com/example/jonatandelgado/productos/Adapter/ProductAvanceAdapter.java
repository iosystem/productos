package com.example.jonatandelgado.productos.Adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.jonatandelgado.productos.Model.Product;
import com.example.jonatandelgado.productos.R;

import java.util.List;

public class ProductAvanceAdapter extends RecyclerView.Adapter<ProductAvanceAdapter.ViewHolder> {

    private List<Product> products;
    private Context context;
    private View.OnClickListener listener;

    public  ProductAvanceAdapter(List<Product> products)
    {
        this.products = products;
        //this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_product,parent, false);
        return  new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Product product = products.get(position);
        holder.name.setText(product.getNombre());
        holder.presio.setText(String.valueOf(product.getPrecio()));
    }

    @Override
    public int getItemCount() {
        return this.products.size();
    }

    public void add(Product product)
    {
        if(!products.contains(product))
        {
            products.add(product);
            notifyDataSetChanged();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder
    {
        private AppCompatTextView presio;
        private AppCompatTextView name;
        private AppCompatImageView imagen;

        ViewHolder(View itemView)
        {
            super(itemView);
            presio = itemView.findViewById(R.id.txtPrice);
            name = itemView.findViewById(R.id.textName);
            imagen = itemView.findViewById(R.id.imgPicture);
        }
    }


}
