package com.example.jonatandelgado.productos.Model;

import com.example.jonatandelgado.productos.DB.DbContext;

public class Category  {

    private long id;
    private String name;
    private int pathImage;

    public Category(){

    }
    public Category(String name, int pathImage) {
        this.name = name;
        this.pathImage = pathImage;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPathImage() {
        return pathImage;
    }

    public void setPathImage(int pathImage) {
        this.pathImage = pathImage;
    }
}
