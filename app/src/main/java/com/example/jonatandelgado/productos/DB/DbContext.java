package com.example.jonatandelgado.productos.DB;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.jonatandelgado.productos.R;

public class DbContext extends SQLiteOpenHelper {


    public DbContext(@Nullable Context context, @Nullable String name,
                     @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        try {
            // Se genera la tabla de User por defecto.
            String queryUserCreate = "create table User (" +
                    "ID integer primary key autoincrement" +
                    ",Name text," +
                    " Pass test)";
            db.execSQL(queryUserCreate);
            // Se genera la tabla de Category por defecto.
            String queryCategoryCreate = "create table Category (" +
                    "ID integer primary key autoincrement" +
                    ",Name text," +
                    "PathImage integer)";
            db.execSQL(queryCategoryCreate);
            // Se genera la tabla de Product por defecto.
            String queryProductCreate = "create table Product (" +
                    "ID integer primary key autoincrement," +
                    "Name text," +
                    "Description text," +
                    "Price real," +
                    "Path_image text," +
                    "ID_Category integer," +
                    "Stock integer," +
                    "foreign key (ID_Category) references Category(ID)" +
                    ")";



            db.execSQL(queryProductCreate);
        }
        catch (Exception ex)
        {
            Log.d("Error DB",ex.getMessage());
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
    public void insertDefaultUser() {
        if (!userDefaultExist())
        {
            ContentValues newInsert = new ContentValues();
            newInsert.put("Name", "admin");
            newInsert.put("Pass", "admin");
            this.getWritableDatabase().insert("User", null, newInsert);
        }
    }
    public void insertDefaultCategory() {
        if (!userDefaultExist())
        {
            ContentValues newInsert = new ContentValues();
            ContentValues newInsert2 = new ContentValues();
            ContentValues newInsert3 = new ContentValues();
            ContentValues newInsert4 = new ContentValues();
            ContentValues newInsert5 = new ContentValues();
            ContentValues newInsert6 = new ContentValues();

            newInsert.put("Name", "Groceries");
            newInsert.put("PathImage", R.drawable.comestibles);
            this.getWritableDatabase().insert("Category", null, newInsert);

            newInsert2.put("Name", "Cars");
            newInsert2.put("PathImage", R.drawable.coche);
            this.getWritableDatabase().insert("Category", null, newInsert2);

            newInsert3.put("Name", "Sport");
            newInsert3.put("PathImage", R.drawable.deportes);
            this.getWritableDatabase().insert("Category", null, newInsert3);

            newInsert4.put("Name", "Beer");
            newInsert4.put("PathImage", R.drawable.cerveza);
            this.getWritableDatabase().insert("Category", null, newInsert4);

            newInsert5.put("Name", "Phones");
            newInsert5.put("PathImage", R.drawable.smartphones);
            this.getWritableDatabase().insert("Category", null, newInsert5);

            newInsert6.put("Name", "TV");
            newInsert6.put("PathImage", R.drawable.television);
            this.getWritableDatabase().insert("Category", null, newInsert6);

        }
    }
    public boolean userDefaultExist() throws SQLException {
        Cursor cursor = null;
        cursor = this.getReadableDatabase()
                .query("User",
                new String []{"Name","Pass"}, "Name = 'admin' and Pass = 'admin'",
                        null,null,null,null
                        );

        return cursor.getCount() > 0? true: false;
    }
    public void OpenDB()
    {
        this.getWritableDatabase();
    }
    public  void CloseDB()
    {
        this.close();
    }
}
