package com.example.jonatandelgado.productos.Model;

import com.example.jonatandelgado.productos.Interface.IProduct;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;

public class Product extends IProduct implements Serializable {

    private String imagen;
    private int codigo;
    private String nombre;
    private Double precio;
    private int cantidad;
    private int cantidad_reorden;
    private int idCategoria;
    private Date fecha_ingreso;
    private boolean activo;
    private String descripcion;

    public Product(){

    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getPrecio() {
        return precio;
    }

    public void setPrecio(Double precio) {
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public int getCantidad_reorden() {
        return cantidad_reorden;
    }

    public void setCantidad_reorden(int cantidad_reorden) {
        this.cantidad_reorden = cantidad_reorden;
    }

    public int getIdCategoria() {
        return idCategoria;
    }

    public void setIdCategoria(int idCategoria) {
        this.idCategoria = idCategoria;
    }

    public Date getFecha_ingreso() {
        return fecha_ingreso;
    }

    public void setFecha_ingreso(Date fecha_ingreso) {
        this.fecha_ingreso = fecha_ingreso;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public Double  buscar_su_precio()
    {
        return  this.getPrecio();
    }
    @Override
    public ArrayList<Object> productos_activos() {
        return null;
    }

    @Override
    public ArrayList<Object> productos_inactivos() {
        return null;
    }

    @Override
    public void buscar_cantidad_de_reorden() {

    }
}
